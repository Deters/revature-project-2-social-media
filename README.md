Project 2 Gitlab for Revature Portfolio


# PROJECT NAME
Revature Social Media Project
## Project Description

This project is a mock social media site with React Front end set up to make a SPA and Spring MVC for back end DB handling for data to 
persist. It implements a Login system with React Front end with the user to interact with their profile and look up others as well with the 
social media standard of interactions of posts and liking them in an old school Facebook fashion. It utilizes Spring MVC to provide endpoints 
so the data can be handled going from front end to back end seemlessly. 

## Technologies Used

* Spring Project set up with dependencies
* Hibernate Spring MVC with database connections
* ARDS database
* Spring MVC Servlet through local host 
* React npm libraries for Front end 
* Axios library, email-js emailing services, React Router, Redux gloabal state manager, Various Bootstrap libraries  
* Java Hash for Password encryption
* Jest and enzyme Testing with React
* Logj4 Logger set up
* JUnit Testing
* DAO OOP Design Pattern through Java 8

## Features

List of features ready and TODOs for future development
* Custom implementation of a spring project 
* React SPA with routing for seemless navigation
* Login checking with verification through spring for unique login for users
* Password protection through hashes
* Email Verification of Password change through email js
* S3 AWS bucket for image management
* Jest and JUnit testing for front end and back end development 

To-do list:
* Implementing comments by others users for posts
* password encryption stored in the database with keys 
* h2 Testing mock database

## Getting Started
   
to start this project use git clone https://gitlab.com/Deters/RevatureProjectNickDeters2020
From here use you may use your prefered IDE however this project was set up with sts and will work well with 
importing the project. https://spring.io/tools#suite-three
To import this Project go to your IDE and select file->import->ExistingMavenproject
From here find the clone project called "revature-project-2-social-media-master -> Backend" select the pom file and import this project. 
There the project should be configured with your own system in the Web.xml file with your own DB credentails as it has system
environement variables that are unique to the team that originally set up the project. From there you should be able to start 
up the Spring project and have a fully functional back end. 

Then setting up the front end with the Front end in the "Front End" folder. We would want to open it in VS code or your front end IDE.
After that run either npm install or your yarn command. From there we can start up the application with npm start and your front end should 
then be fully functional. 

## Usage

To use this we just need to start up React with npm start. After that you be inside the login part of the website. From here you are able to 
Register as a new user and login to see the feed. What is renedered on the front always is the header that provides a home link and a drop down
this drop down allows you to interact with it such as creating a post, going to a home feed of everyone posts, as well as the routes to change 
your information. Also you can make a post through a model and see them in cards rendered in feeds from the page. Also the search bar is added
on the header allowing you to search for any other users either created by you or someone else and go directly to their profile and see their 
specific posts. Lastly an S3 bucket and email js features can also be used to send emails when changing your password and S3 bucket for storing
images either in your post or having a unique profile picture for yourself. 

## Contributers
Andrew Keller, Justin Wen, Mark Antony Vargus, William Stone, Nicholas Deters
